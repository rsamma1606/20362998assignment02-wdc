/**
 * This file contains the Home component.
 * Other React components for viewing notes and notebooks should be nested
 * beneath the Home component.
 */

const React = require('react');

const NotebookList = require('./NotebookList');

/*
  *** TODO: Start building the frontend from here ***
  You should remove the placeholder text and modify the component as you see
  fit while working on the assignment.
*/
const Home = () => (
  <div>
    {/* The heading area of the page */}
    <div className="blog-header">
      <h1 className="blog-title">An Example of a Blog</h1>
      <p className="lead blog-description">React and Redux and Bootstrap, oh my!</p>
    </div>
    {/* A list of blog posts, including a couple of buttons */}
    <NotebookList />
  </div>
);

module.exports = Home;
