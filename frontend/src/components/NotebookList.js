const React = require('react');
const ReactRedux = require('react-redux');
const _ = require('lodash');

const ActiveNotebook = require('./ActiveNotebook');
const Notebook = require('./NoteBook');
const notesActionCreators = require('../reducers/notes');
const createActionDispatchers = require('../helpers/createActionDispatchers');
const notebooksActionCreators = require('../reducers/notebooks');
const NotebookNew = require('./NoteBookNew');

/*
  *** TODO: Build more functionality into the NotebookList component ***
  At the moment, the NotebookList component simply renders the notebooks
  as a plain list containing their titles. This code is just a starting point,
  you will need to build upon it in order to complete the assignment.
*/
class NotebookList extends React.Component {
constructor(props) {
    super(props);
    this.state = { loading: false };
  }
  render() {
    const createNotebookComponent = (notebook) => {

      if(notebook.id == this.props.notebooks.activeNotebookId){
        console.log(notebook.id);
        return <ActiveNotebook
          key = {notebook.id}
          notebook = {notebook}
          notes = {this.props.notes.notes}
          setActiveNotebook = {this.props.setActiveNotebook}
          deleteNotebook = {this.props.deleteNotebook}
          activeNote = {this.props.activeNote}
        />
      }
      return (
        <Notebook
          key={notebook.id}
          notebooktitle={notebook}
          saveNotebooks ={this.props.saveNotebook}
          loadNotes = {this.props.loadNotes}
          deleteNotebook={this.props.deleteNotebook}
        />
      );
    };

    return(
      <div className="row">
        <div className="blog-main">
            <NotebookNew
              createNotebook={this.props.createNotebook}
            />
            {this.props.notebooks.visibleNotebooks.map(createNotebookComponent)}
        </div>
      </div>
            );
          }
        }

const NotebookListContainer = ReactRedux.connect(
  (state) => ({
    notebooks: state.notebooks,
    activeNotebookId: state.activeNotebookId,
    notes: state.notes
  }),
  createActionDispatchers(notebooksActionCreators,notesActionCreators)
)(NotebookList);

module.exports = NotebookListContainer;
