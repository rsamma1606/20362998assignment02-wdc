const _ = require('lodash');
const api = require('../helpers/api');

// Action type consts
const INSERT_NOTE = 'INSERT_NOTE';
const REMOVE_NOTE = 'REMOVE_NOTE';
const GET_NOTES = 'GET_NOTES';


const initialState = {
    data: [],

};

function reducer(state, action) {
    state = state || initialState;
    action = action || {};

    switch (action.type) {
        case INSERT_NOTE: {
            const mergedNotes = _.concat(state.data, action.notes);
            const orderedNotes = _.orderBy(mergedNotes, 'createdAt', 'DESC');
            return _.assign({}, state, {data: orderedNotes});
        }
        case REMOVE_NOTE: {
            const newNotes = _.reject(state.data, {noteId: action.id});
            return _.assign({}, state, {data: newNotes});
        }
        case GET_NOTES: {
            return _.assign({}, state, {data: action.notes});
        }

        default:
            return state;
    }
}

// Action Creators
reducer.insertNote = (note) => {
    return {
        type: INSERT_NOTE,
        note
    };
};

reducer.removeNote = (noteId) => {
    return {
        type: REMOVE_NOTE,
        noteId
    };
};

module.exports = reducer;
